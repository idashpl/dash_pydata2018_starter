import plotly.graph_objs as go
from data_prep import mod_flights, airlines, static_sum_flights

def generate_plot():

    agg_data = mod_flights\
            .query(f"carrier in {['UA', 'AA']}") \
            .groupby(['name', 'hour'])\
            .apply(static_sum_flights)\
            .reset_index()\
            .rename(columns = {'name': 'Airline', 'hour': 'Hour', 0: 'Departure delay'})

    traces = []
    for i in agg_data['Airline'].unique():
      df_by_airline = agg_data[agg_data['Airline'] == i]
      traces.append(go.Bar(
          x=df_by_airline['Hour'],
          y=df_by_airline['Departure delay'],
          name=i
      ))

    return {
    'data': traces,
    'layout': go.Layout(
            xaxis={'title': 'Hour'},
            yaxis={'title': 'Percentage of delayed flights'},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest'
        )
    }
