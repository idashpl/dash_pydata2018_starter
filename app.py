# Load packages
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
import numpy as np
import plotly.graph_objs as go
from data_prep import mod_flights, airlines

# Pull stylesheet from remote location
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# Initialize the app
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# Create layout
app.layout = html.Div(
  [
    html.Div(
      html.Img(src='assets/logo_black.png', width='200'),
      style = {'marginBottom' : '20px', 'width' : '100%'}
    )
  ],
  className = 'container'
)

# Run the app
if __name__ == '__main__':
  app.run_server(debug=True)
