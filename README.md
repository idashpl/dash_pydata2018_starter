
## Installation

Run `. install_dependencies.sh`. It will create conda environment with required modules.

## Content

- app.py - a script with dash code
- data_prep.py - data pre-processing tasks
- static_elements.py - function which create a plot.
- assets - directory contains all necessary `css` files and images.
