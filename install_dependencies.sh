# Create a full "anaconda" environment using conda.
conda create -n 2018_pydata_dash python=3.6

# Activate the environment for subsequent installs
source activate 2018_pydata_dash

conda install pandas
conda install numpy
pip install dash==0.30.0
pip install dash-html-components==0.13.2
pip install dash-core-components==0.38.0
pip install dash-table==3.1.6
pip install plotly
