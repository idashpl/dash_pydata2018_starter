import pandas as pd
import numpy as np

# Read and pre process the data
flights = pd.read_csv('data/flights.csv')
airlines = pd.read_csv('data/airlines.csv')

mod_flights = pd.merge(flights, airlines, on = 'carrier')
mod_flights = mod_flights[mod_flights.dep_delay.notnull()]

def static_sum_flights(grouped_df):
    dep_delay = grouped_df['dep_delay']
    return np.sum(dep_delay > 0) / len(grouped_df)
